﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraCompartida : MonoBehaviour
{
    public Transform[] objetivos;
    private Vector3 desplazamiento;
    public float altura = 5.0f;
    private Camera cam;
    void Start()
    {
        cam = GetComponent<Camera>();
        ManagerMusica m = ManagerMusica.GetInstancia();

    }
    public float velocidad = 1.0f;
    public float camaraSizeMax = 100.0f;
    public float camaraSizeMin = 2.0f;
    public float extraSizeCamara = 2.0f;
    void FixedUpdate()
    {
        transform.rotation = Quaternion.LookRotation(Vector3.down, Vector3.forward);
        Vector3 prom = Vector3.zero;
        foreach (Transform o in objetivos)
        {
            prom += o.position;
        }
        prom /= objetivos.Length;
        prom += Vector3.up * altura;

        Vector3 objetivoFinal = prom;
        transform.position = Vector3.Lerp(transform.position, objetivoFinal, velocidad * Time.deltaTime);

        float maxAltura = 0.0f;
        float maxAncho = 0.0f;
        foreach (Transform o in objetivos)
        {
            float dAltura = Mathf.Abs(transform.position.z - o.position.z);
            float dAncho = Mathf.Abs(transform.position.x - o.position.x);
            if (dAltura > maxAltura)
            {
                maxAltura = dAltura;
            }
            if (dAncho > maxAncho)
            {
                maxAncho = dAncho;
            }
        }
        maxAncho = maxAncho / cam.aspect;
        if (maxAltura > maxAncho)
        {
            cam.orthographicSize = Mathf.Clamp(maxAltura + extraSizeCamara, camaraSizeMin, camaraSizeMax);
        }else
        {
            cam.orthographicSize = Mathf.Clamp(maxAncho + extraSizeCamara, camaraSizeMin, camaraSizeMax);
        }
        
    }
}
