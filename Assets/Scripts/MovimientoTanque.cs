﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoTanque : MonoBehaviour
{
    private Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private float inputAdelante;
    private float inputRotacion;
    public string ejeRotacion = "Horizontal1";
    public string ejeAdelante = "Vertical1";

    public AudioClip sonidoQuieto;
    public AudioClip sonidoMoviendose;
    public AudioSource audio;
    void Update()
    {
        inputRotacion = Input.GetAxis(ejeRotacion);
        inputAdelante = Input.GetAxis(ejeAdelante);
    }
    public float velocidadRotacion = 1.0f;
    public float velocidadLineal = 1.0f;
    public bool mueveYRota = true;
    private void FixedUpdate()
    {
        if (!mueveYRota)// Solo rotacion o desplazamiento a la vez?
        {
            if(Mathf.Abs(inputRotacion) > 0.0f)// Prioridad a rotacion
            {
                rb.angularVelocity = new Vector3(0.0f, velocidadRotacion * inputRotacion, 0.0f);
            }
            else// Dejamos que avance
            {
                rb.velocity = inputAdelante * transform.forward * velocidadLineal + Vector3.up * rb.velocity.y;
            }
        }
        else
        {
            rb.angularVelocity = new Vector3(0.0f, velocidadRotacion * inputRotacion, 0.0f);
            rb.velocity = inputAdelante * transform.forward * velocidadLineal + Vector3.up * rb.velocity.y;
        }

        if ((rb.velocity.magnitude > 0.1f) || (rb.angularVelocity.magnitude > 0.1f))
        {
            if (audio.clip != sonidoMoviendose)
            {
                audio.clip = sonidoMoviendose;
                audio.Play();
            }
            audio.pitch = Mathf.Lerp(1.0f, 1.3f, Mathf.InverseLerp(5.0f, 10.0f, rb.velocity.magnitude));
            //Debug.Log("Pitch " + audio.pitch);
        }
        else
        {
            if (audio.clip != sonidoQuieto)
            {
                audio.clip = sonidoQuieto;
                audio.Play();
            }
        }
    }
}
