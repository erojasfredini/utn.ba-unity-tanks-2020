﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torre : MonoBehaviour
{
    public Transform torre;
    public GameObject prefabBala;
    public Transform origenBala;
    public AudioSource audio;
    public AudioClip sonidoDisparo;
    public ParticleSystem explosionDisparo;
    void Start()
    {
    }
    private float dRot;
    public string inputRotacion = "Torre1";
    public string inputDisparo = "Fire1";
    public float velocidadInicialBala = 5.0f;
    public float coolDown = 1.0f;
    private float tiempoCoolDown = 0.0f;
    public float distMinimo = 1.0f;
    private Vector3 objetivoTorre;
    private float alpha;
    void Update()
    {
        var c = Camera.main;
        var rayo = c.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        int numCapa = LayerMask.NameToLayer("Piso");
        int mascara = 3 << numCapa;
        if (Physics.Raycast(rayo.origin, rayo.direction, out hit, 1000.0f, mascara))
        {
            var posTanque = transform.position;
            posTanque.y = 0.0f;
            var posPiso = hit.point;
            posPiso.y = 0.0f;
            var dist = (posTanque - posPiso);
            if (dist.magnitude > distMinimo)
            {
                float h = Vector3.Dot(dist, torre.forward);
                float w = Vector3.Dot(dist, torre.right);
                alpha = Mathf.Atan2(h, w) + Mathf.PI / 2.0f;
                Debug.Log("alpha " + alpha * Mathf.Rad2Deg);
                objetivoTorre = hit.point;
                //Debug.Log("Colisiono con " + hit.collider.name + " en la posicion " + hit.point);
            }
        }

        dRot = Input.GetAxis(inputRotacion);
        bool disparo = Input.GetButtonDown(inputDisparo);
        if (disparo && (tiempoCoolDown == 0.0f))
        {
            GameObject balaCreada = GameObject.Instantiate(prefabBala, origenBala.position, origenBala.rotation);
            Rigidbody balaRB = balaCreada.GetComponent<Rigidbody>();
            balaRB.velocity = balaRB.transform.forward * velocidadInicialBala;
            tiempoCoolDown = coolDown;
            //audio.PlayOneShot(sonidoDisparo);
            audio.Play();
            explosionDisparo.Play();
        }
        tiempoCoolDown -= Time.deltaTime;
        tiempoCoolDown = Mathf.Max(0.0f, tiempoCoolDown);
    }
    public float velocidad = 1.0f;
    private void FixedUpdate()
    {
        //torre.transform.Rotate(Vector3.up * velocidad * dRot * Time.deltaTime);
        //var rotObjetivo = Quaternion.LookRotation(objetivoTorre - torre.position, Vector3.up);
        //torre.transform.rotation = Quaternion.Slerp(torre.transform.rotation, rotObjetivo, Time.deltaTime * velocidad);
        torre.transform.Rotate(Vector3.up * -alpha * velocidad * Time.deltaTime);
    }
}
