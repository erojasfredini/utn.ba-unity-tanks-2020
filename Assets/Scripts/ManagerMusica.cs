﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerMusica
{
    private static ManagerMusica instancia = null;

    private ManagerMusica()
    { }

    public static ManagerMusica GetInstancia()
    {
        if (instancia == null)
        {
            instancia = new ManagerMusica();
        }
        return instancia;
    }
}
