﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject prefabExplosion;
    public LayerMask layer;
    public float radioExplosion = 1.0f;
    public float fuerzaExplosion = 1.0f;
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
        {
            var explosion = GameObject.Instantiate(prefabExplosion, transform.position, Quaternion.Euler(0.0f, Random.Range(0.0f, 360f), 0.0f));
            var fx = explosion.GetComponent<ParticleSystem>();
            fx.Play();
            Collider[] cols = Physics.OverlapSphere(transform.position, radioExplosion, layer);
            for (int c=0; c < cols.Length; ++c)
            {
                var r = cols[c].GetComponent<Rigidbody>();
                if (r != null)
                {
                    r.AddExplosionForce(fuerzaExplosion, transform.position, radioExplosion);
                }
            }
            GameObject.Destroy(gameObject);
        }
    }
}
